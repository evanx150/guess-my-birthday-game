from random import randint

name = input("Hi! What is your name?")
month = randint(1, 12)
year = randint(1980, 2003)
n = 0

print("Guess#1: " + str(name) + " were you born in " + str(month) + " / " + str(year))
answer = input("yes or no?")

for x in range(5):
    if answer == "yes":
        print("I knew it!")
        break
    elif answer == "no":
        n += 1
        y = n + 1
        if n == 5:
            print("I have other things to do. Good bye.")
        else:    
            print("Drat! Lemme try again!")
            month = randint(1, 12)
            year = randint(1980, 2003)
            print("Guess#" + str(y) + ":" + name + " were you born in " + str(month) + " / " + str(year))
            answer = input("yes or no?")
